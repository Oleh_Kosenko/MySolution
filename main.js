const fs = require("fs");
const path = require("path");
const { JSDOM } = require("jsdom");

const htmlFilesPath = "./fileshtml";

try {
    const htmlFiles = fs.readdir(htmlFilesPath, (err, list) => {
        if (err) throw err;

        list = list.filter(fileName => {return (fileName.indexOf(".html") > -1 && fileName.indexOf(".html") === (fileName.length - 5))})
        list.forEach(filePath => {
            console.log(`-- ${filePath} ------------`);
            const file = fs.readFileSync(path.join(htmlFilesPath, filePath));
            const dom = new JSDOM(file);

            const queryRes = [...dom.window.document.querySelectorAll(".panel-heading")];
            const makeEverythingOKArea = (queryRes.filter(el => {
                return (el.innerHTML.indexOf("Make Everything OK Area") > -1);
            }))[0];

            let buttons = [...makeEverythingOKArea.parentNode.querySelectorAll("a")];

            buttons = buttons.filter(button => {
                let ok = false;

                for( let attr of  [...button.attributes]) {
                    if ((attr.name == "onclick" && attr.value.indexOf("ok") > -1)) {
                            ok = true;
                            break;
                        }
                    }
                    return ok;
            })

            buttons.forEach(button => {
                console.log([...button.attributes].map(attr => `${attr.name} = ${attr.value}`).join(", "));
                console.log("button text:", button.innerHTML.trim());
            })
        })
    })
} catch (err) {
    console.log(("Error trying to find element by css selector", err));
}
